import numpy as np
import os
from itertools import product
from scipy import interpolate
from scipy.interpolate import interp1d
from scipy.optimize import curve_fit
from screening import vdWH

from transport.epc.my_tools import get_chi0

import constants as cte

bohr_to_ang = 0.529177211
eVtoRy = 1.0/13.605698

DBdir = os.path.dirname(os.path.realpath(__file__)) +"/DB"


def Fro_pot(H,mat,zs,zmat,C):
    xzs = H.params[mat]["zprof"]+zmat
    dz = H.params[mat]["dz"]
    V = np.zeros((len(H.qs), len(zs)))
    for iq, q in enumerate(H.qs):
        f = H.f[mat][iq]
        for iz, z in enumerate(zs):
            V[iq, iz] = C*np.sum(np.exp( -q*np.abs(z-xzs) ) * f )*dz
    return V

def get_fro_pert(H, mat, qs, Cs):
    """
    Get the input perturbation from the bare frolich perturbation in each layer.

    H is the heterostructure instance.
    qs is the vector of q
    Cs is the dictionnary of the Frohlich constants. It defines which layer generates a Frohlich potential,
    and its strength. The "strength" is the q->0 limit of the el-ph coupling in eV.
    for example Cs = {"BN": 2.,"MoS2": 0.3}

    For each layer, get z,zk,  dz
    for each q, get f(q,z), Vext = sum(Vfro from each layer)
    integrate.
    """
    W = np.zeros((len(qs), 2*H.Nlayers))
    for ik, matk in enumerate(H.layers):
        zk = H.zlayers[ik]
        zfgk = H.params[H.layers[ik]]["zprof"]
        dzk = H.params[H.layers[ik]]["dz"]
        Vfros = sum([Fro_pot(H, mat, zfgk+zk, zl, Cs[H.layers[il]])
                    for il, zl in enumerate(H.zlayers) if H.layers[il]==mat ])
        #np.sum(np.array([Fro_pot(H, matk, zfgk+zk, zl, Cs[H.layers[il]])
        #    for il, zl in enumerate(H.zlayers)]), axis=0)
        for iq, q in enumerate(qs):
            fk = H.f[H.layers[ik]][iq]
            gk = H.g[H.layers[ik]][iq]
            #Vextf = sum([Vsq(q,zfgk+zk,zl, Cs[H.layers[il]]) for il, zl in enumerate(H.zlayers)])
            W[iq, ik] = np.sum(Vfros[iq]*fk)*dzk #Vextf
            #Vextg = sum([Vsq(q,zfgk+zk,zl, Cs[H.layers[il]])for il, zl in enumerate(H.zlayers)])
            W[iq, H.Nlayers+ik] = np.sum(Vfros[iq]*gk)*dzk #
    return W

def FD(ef, ek, kT):
    # fermi-dirac occupations
    return 1.0/(1+np.exp((ek-ef)/kT))

def MV(ef, ek, kT):
    # marzari vanderbilt occupations
    x=(ek-ef)/kT
    return 0.5*(np.sqrt(2/ np.pi) * np.exp( -x**2 - np.sqrt(2) * x-1./2 ) + 1 - erf( x + 1./np.sqrt(2) ))

# free-carrier screening

def n_of_e(bands_dir, kT, occ = "FD"):
    """
    returns Fermi level and corresponding densities for
    eF between elow and ehigh.
    bands_folder: location of bands data
    ibnd: index of conduction band
    kT : temperature in eV
    Simplified version from the one in the transport module.
    Original version requires AiiDA and the valleys object.
    This version is specific to conduction bands (as opposed to valence bands)
    and without spin-orbit (each state is double degenerate).

    """
    if occ == "FD":
        f = FD
    elif occ == "MV":
        f = MV
    else:
        print("not implemented")
        return
    BZinfo = np.loadtxt(bands_dir+'/BZinfo.txt')
    NBZ = int(BZinfo[0])
    SR = BZinfo[1]
    bands = np.load(bands_dir+'/bands.npy')
    ks = bands[:,:2]
    eks = bands[:,2]
    elow = np.min(eks) - 0.1
    ehigh = np.max(eks)
    efs = np.linspace(elow, ehigh, 200)
    ns = np.zeros(efs.shape[0])
    for i,ef in enumerate(efs):
        n = 0
        n += 2*np.sum(f(ef,eks, kT))/NBZ/SR
        ns[i] = n
    return efs, ns/5.29177e-9**2 # from bohr-2 to cm-2

def NumChi0(bands_dir, xqs, kT, ef, occ = 'FD', dir = [1.,0.,0.]):
    """
    compute chi0 numercially with the bands
    """
    dir = np.array(dir)
    #SBZ = np.cross(rec_cell[0], rec_cell[1])[2] #surface of the Brillouin zone
    #NBZ = # number of points in the Brillouin zone.
    if occ == "FD":
        f = FD
    elif occ == "MV":
        f = MV
    else:
        print("not implemented")
        return
    BZinfo = np.loadtxt(bands_dir+'/BZinfo.txt')
    NBZ = int(BZinfo[0])
    SR = BZinfo[1]
    bands = np.load(bands_dir+'/bands.npy')
    ks = bands[:,:2]
    eks = bands[:,2]
    m = np.max(eks)+0.1
    F = np.zeros(len(xqs))
    for iq, q in enumerate(xqs):
        kqs = ks + q*dir
        ekqs = interpolate.griddata(ks,eks,kqs,method='cubic', fill_value=m)
        fks = f(ef, eks, kT)
        fkqs = f(ef, ekqs, kT)
        F[iq] +=  2*np.sum((fks-fkqs)/(ekqs-eks))/eVtoRy/SR/NBZ #in states/Ry/bohr**2
    return -F

def free_carrier(H, xmat, bands_dir, ef, kT = 0.026, occ='FD', pathto_data = DBdir):
    """
    replace the response of xmat with free carrier screening modeled by chi
    """
    print("computing monolayer response...")
    #FF from the neutral material... should be improved if possible
    Sff = H.get_form_factors()[xmat]
    # compute chi to inject back into screening model
    # to combine diel and free carrier response, work with
    # the chi_0. THey are additive!
    vc = vc = 4*np.pi/H.qs
    chi0f = NumChi0(bands_dir, H.qs, kT, ef, occ = occ, dir = [1.,0.])*Sff
    chid = H.Q[xmat]*Sff
    chi0d = chid/(1+vc*chid)
    chi0 = chi0d + chi0f
    chi = chi0 / (1-vc*chi0)
    print("added free carrier response and put it back into VdWH")
    # chi_int = Q*form factors
    H.Q[xmat] = chi/Sff
    H.Qf[xmat] = np.array([c*H.f[xmat][iq] for iq, c in enumerate(H.Q[xmat])])
    return
