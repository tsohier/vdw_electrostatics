import numpy as np
import os
from itertools import product
from scipy.interpolate import interp1d
from scipy.optimize import curve_fit


bohr_to_ang = 0.529177211

DBdir = os.path.dirname(os.path.realpath(__file__)) +"/DB"

class vdWH(object):
    """
    The vdWH object represents the van der Waals heterostructure in which
    we solve the electrostatics.
    Initialize with:
    layers: a list of string, represent the names of the successive layers,
            as in the database, i.e. 'GaSe', 'BN', or 'Gr1e13'.
            for example, bilayer BN is ['BN', 'BN'], GaSe/BN/graphene is
            ['GaSe', 'BN', or 'Gr1e13'].
    dists = a list of Nlayers-1 floats to indicate the distance between each
            layer. This is the distance between the center of the layers.
    pathto_data = the path to the database containing the ab initio responses.

    """

    def __init__(self, layers, dists, pathto_data=DBdir):
        # layers is a list of the different materials
        self.layers = layers
        self.Nlayers = len(layers)
        self.mats = list(set(layers))
        #self.tpoa = 2*np.pi/alat
        # distances betweeen the layers # should be Nlayers -1
        self.dists = dists
        self.params = self.collect_params(pathto_data, self.mats)
        #
        z_layers=[sum(dists[:i])-sum(dists)/2 for i in range(self.Nlayers)]
        if dists == [] : z_layers = [0.0]
        self.zlayers = z_layers
        # initialze some vairables that will be computed along the way
        self.P  = None
        self.Q = None
        self.W = None
        self.solved = False

    def set_qpoints(self, qs):
        self.qs = qs*bohr_to_ang 
        self.interp_PQ()
        # do not solve just yet, let the opportunity to the user to set a
        # different perturbation
        #self.solve()
        print("qpoints set, parameters interpolated" )
        return

    def collect_params(self, pathto_data, mats):
        """
        collect the raw responses from folder
        """
        params={}
        for mat in mats:
            f = os.path.join(pathto_data, mat)
            qpts = np.load(f+"/qpoints.npy")
            zs = np.load(f+"/z.npy")
            monoresp = np.load(f+"/monopole.npy")
            dipresp = np.load(f+"/dipole.npy")
            # dipole response depends on the sign of the electric field used in the DFT calc.
            # we should fix it at the DFT level, but just in case, since those are all conventions anyhow,
            # we can fix it here in case we got the wrong sign. The response is supposed to be opposite sign form the excitation
            # which by convention in our framework is z-z0. so int(resp*(z-z0)) is supposed to be negative.
            if np.sum(dipresp[-1,:]*zs)>0:
                dipresp = - dipresp
                print("correcting sign")
            params[mat] = {"zprof":zs, "dz" :zs[1]-zs[0] ,"qpoints":qpts, "mono":monoresp, "dip":dipresp} # zprof stands for "z profile"
        return params

    def interp_PQ(self):
        """
        Interpolate the models' parameters on the set of q points,
        provided in input, starting from the raw density responses contained in
        the databse and stored in self.params after initialization of VdWH.
        The notation follows the paper.
        """
        # {material:parameters} dictionnaries
        P = {}
        Q = {}
        Pg = {} # P(q)g(q,z-z0)
        Qf = {} # Q(q)f(q,z-z0)
        f = {}
        g = {}
        for mat, par in list(self.params.items()):

            monoint = interp1d(par["qpoints"],par["mono"], axis=0, kind='quadratic', bounds_error=False, fill_value="extrapolate")
            dipint = interp1d(par["qpoints"],par["dip"], axis=0, kind='quadratic', bounds_error=False, fill_value="extrapolate")
            # Qf[iq, iz]
            Qf[mat] = monoint(self.qs)
            # Pg[iq, iz]
            Pg[mat] = dipint(self.qs)
            # Q[iq]
            Q[mat] = np.sum(Qf[mat], axis = 1 )* par['dz']
            #P[iq]
            P[mat] = np.sum(Pg[mat]*par["zprof"], axis=1)* par['dz']
            #
            f[mat] = np.divide(Qf[mat].T,Q[mat]).T
            #
            g[mat] = np.divide(Pg[mat].T,P[mat]).T
            if np.max(np.abs(P[mat]))<1e-12:
                g[mat] = np.zeros(g[mat].shape)
        self.P = P
        self.Q = Q
        self.Pg = Pg
        self.Qf = Qf
        self.f = f
        self.g = g
        return

    def compute_FGH(self, iq, q):
        """
        Computing the convolution/double integrals.
        Noted C[f,g] in the paper.
        C[f,f] => F
        C[g,g] => G
        C[f,g] => H
        """
        F = np.zeros((self.Nlayers, self.Nlayers))
        G = np.zeros((self.Nlayers, self.Nlayers))
        H = np.zeros((self.Nlayers, self.Nlayers))
        #
        for ip, matp in enumerate(self.layers):
            for ik, matk in enumerate(self.layers):
                # we compute the only the conjugates
                if ik<ip:
                    # layers k
                    # we assume that the z grid is the same for Pf and Qg...
                    xzk = self.params[matk]["zprof"]
                    dzk = self.params[matk]["dz"]
                    fk = self.f[matk][iq]
                    gk = self.g[matk][iq]
                    # for layer p
                    xzp = self.params[matp]["zprof"]
                    dzp = self.params[matp]["dz"]
                    fp = self.f[matp][iq]
                    gp = self.g[matp][iq]
                    #
                    zp = self.zlayers[ip]
                    zk = self.zlayers[ik]
                    # definning ip, ik rather than ik, ip because that is what is written in the model
                    # note that getting the right convolution is tricky, with the position of the reshape...
                    F[ip,ik] = np.sum(np.sum(np.exp(-q*np.abs(xzp+zp-(xzk.reshape(len(xzk), 1)+zk)))*fp, axis=1)*dzp*fk)*dzk
                    #F[ip,ik] = np.sum(np.sum( np.exp(-q*np.abs(xzp.reshape(len(xzp),1)+zp-(xzk+zk)))*fp, axis=1)*dzp*fk)*dzk
                    F[ik, ip] = F[ip,ik]
                    H[ip,ik] = np.sum(np.sum(np.exp(-q*np.abs(xzp+zp-(xzk.reshape(len(xzk), 1)+zk)))*fp, axis=1)*dzp*gk)*dzk
                    #H[ip,ik] = np.sum(np.sum(np.exp(-q*np.abs(xzp.reshape(len(xzp), 1)+zp-(xzk+zk)))*fp, axis=1)*dzp*gk)*dzk
                    H[ik,ip] = np.sum(np.sum(np.exp(-q*np.abs(xzp+zp-(xzk.reshape(len(xzk), 1)+zk)))*gp, axis=1)*dzp*fk)*dzk
                    #H[ik,ip] = np.sum(np.sum(np.exp(-q*np.abs(xzk.reshape(len(xzk), 1)+zk-(xzp+zp)))*fk, axis=1)*dzk*gp)*dzp
                    G[ip,ik] = np.sum(np.sum(np.exp(-q*np.abs(xzp+zp-(xzk.reshape(len(xzk), 1)+zk)))*gp, axis=1)*dzp*gk)*dzk
                    #G[ip,ik] = np.sum(np.sum(np.exp(-q*np.abs(xzp.reshape(len(xzp), 1)+zp-(xzk+zk)))*gp, axis=1)*dzp*gk)*dzk
                    G[ik,ip] = G[ip,ik]
        self.F = F
        self.G = G
        self.H = H
        return

    def prefactors_av(self, q, iq, ik, ip):
        """
        here we compute the prefactors of each term in the system of 2N
        equations for the electrostatics
        """
        # for layer k
        tpoq = 4.0*np.pi / q #  2 pi e2 in Ry
        #
        Pq = self.P[self.layers[ik]][iq]
        Qq = self.Q[self.layers[ik]][iq]
        #
        Pvv = tpoq * Qq * self.F[ip, ik]
        Pvdv = tpoq * Pq * self.H[ip, ik]
        Pdvv = tpoq * Qq * self.H[ik, ip]
        Pdvdv = tpoq * Pq * self.G[ip, ik]
        return Pvv, Pvdv, Pdvv, Pdvdv

    def Tqmatrix(self, q, iq):
        """
        compute the T matrix at a given q. The T matrix links
        induced [V,DV] to total [V+W, dV+dW]. It's just the matrix
        representation of the system of equations.
        V is the induced potential (by all other layers) averaged over the monopole profile
        dV is the induced potential averaged over dipole profile ( related to derivative )
        W, dW: smae for perturbation potential.
        # Mxy is such that X = Mxy Y
        """

        Mvv = np.zeros((self.Nlayers,self.Nlayers))
        Mvdv = np.zeros((self.Nlayers,self.Nlayers))
        Mdvv = np.zeros((self.Nlayers,self.Nlayers))
        Mdvdv = np.zeros((self.Nlayers,self.Nlayers))
        #
        for ip, matp in enumerate(self.layers):
            for ik, matk in enumerate(self.layers):
                if ik != ip:
                    Pvv, Pvdv, Pdvv, Pdvdv = self.prefactors_av(q,iq,ik,ip)
                    Mvv[ip][ik] = Pvv
                    Mvdv[ip][ik] = Pvdv
                    Mdvv[ip][ik] = Pdvv
                    Mdvdv[ip][ik] = Pdvdv
        return np.row_stack((np.column_stack((Mvv,Mvdv)), np.column_stack((Mdvv,Mdvdv))))

    def solve(self, W = None):
        """
        Just solving the linear system of equations
        """
        print("solving...")
        # interpolate params on qs if not already done
        if self.Q is None: self.interp_PQ()
        # W is W[iq,ik], the perturbation
        # set the
        if W is None:
            W = np.zeros((len(self.qs), 2*self.Nlayers))
            W[:, :self.Nlayers] = 1.
        self.W = W
        # Id-T X = T W
        # X = inv(Id-T)* T*W
        # I save the solution X
        X = np.zeros((len(self.qs), 2*self.Nlayers))
        for iq, q in enumerate(self.qs):
            self.compute_FGH(iq,q)
            T = self.Tqmatrix(q,iq)
            invImT = np.linalg.inv(np.eye(2*self.Nlayers,2*self.Nlayers)-T)
            X[iq] = np.dot(invImT,np.dot(T, W[iq]))
        self.sol = X
        self.solved = True
        print("solved")
        return

    def prefactors_Vind(self, q, iq, ik,z):
        """
        here we compute the prefactors of each term in the expression of the
        induced potential generated by layer k, at q. v^{k}_{ind}(q,z)
        """
        #
        tpoq = 4.0*np.pi / q # actually 2 pi e2 in Ry
        Qf = self.Qf[self.layers[ik]][iq,:]
        Pg = self.Pg[self.layers[ik]][iq,:]
        # we assume that the z grid is the same for Pf and Qg...
        xz = self.params[self.layers[ik]]["zprof"]
        dz = self.params[self.layers[ik]]["dz"]
        zk = self.zlayers[ik]
        #Pq = self.P[self.layers[ik]][iq]
        #Qq = self.Q[self.layers[ik]][iq]
        Pvv = tpoq * np.sum(np.exp(-q*np.abs(z-(xz+zk)))*Qf) * dz
        Pvdv = tpoq *  np.sum(np.exp(-q*np.abs(z-(xz+zk)))*Pg) * dz
        return Pvv, Pvdv

    def get_Vind(self, zs, ilayers = None):
        """
        compute total induced potential, or the sum over layers
        specified by indices ilayers if defined.
        """
        if ilayers is None: ilayers = range(len(self.layers))
        if not self.solved: self.solve(self.qs, W)
        Vind = np.zeros((len(self.qs), len(zs)))
        dVind = np.zeros((len(self.qs), len(zs)))
        for iq, q in enumerate(self.qs):
            for iz, z in enumerate(zs):
                for ik in ilayers:
                    Pvv, Pvdv = self.prefactors_Vind(q, iq, ik,z)
                    Vind[iq, iz] += Pvv * ( self.sol[iq][ik] + self.W[iq][ik] )
                    Vind[iq, iz] += Pvdv * ( self.sol[iq][self.Nlayers+ik] + self.W[iq][self.Nlayers+ik] )
                    #if iq == len(self.qs)-1: print iz, Pvdv*(self.sol[iq][self.Nlayers+ik]+self.W[iq][self.Nlayers+ik])
        return Vind

    def get_zs(self, points_per_layer=51):
        """
        generate a vector of z values
        """
        zmin = min([np.min(self.params[layer]["zprof"]) for layer in self.layers])
        zmax = max([np.max(self.params[layer]["zprof"]) for layer in self.layers])
        zs = np.linspace(min(self.zlayers)+zmin, max(self.zlayers)+zmax, points_per_layer*self.Nlayers)
        return zs

    def potential_average(self, V, zV, ilayers = None):
        """
        average the potential over all layers, or those specified by
        indices ilayers
        """
        if ilayers is None:
            ilayers = list(range(len(self.layers)))
        Vave = np.zeros(len(self.qs))
        for ik in ilayers:
            for iq, (q, vq) in enumerate(zip(self.qs,V)):
                xz = self.params[self.layers[ik]]["zprof"]
                dz = self.params[self.layers[ik]]["dz"]
                fk = self.f[self.layers[ik]][iq]
                vint = interp1d(zV,vq, kind = 'cubic')(xz+self.zlayers[ik])
                Vave[iq] += np.sum(fk*vint)*dz / len(ilayers)
        return Vave

    def get_eps(self, W = None, ilayers = None):
        """
        compute dielectric function as a function of q.
        Beware: makes sense if the perturbation is
        the default, monopole perturbation.
        """
        if not self.solved: self.solve(W = W)
        zs = self.get_zs()
        Vind = self.get_Vind(zs)
        epsinv = np.ones(len(self.qs)) + self.potential_average(Vind,zs, ilayers = ilayers)
        return 1./epsinv

    def get_form_factors(self):
        """
        Compute the form factors.
        """
        form_factors = {}
        for ik, matk in enumerate(self.layers):
            FF = np.zeros(len(self.qs))
            for iq, q in enumerate(self.qs):
                # layers k
                # we assume that the z grid is the same for Pf and Qg...
                xzk = self.params[matk]["zprof"]
                dzk = self.params[matk]["dz"]
                fk = self.f[matk][iq]
                zk = self.zlayers[ik]
                FF[iq] = np.sum(np.sum(np.exp(-q*np.abs(xzk+zk-(xzk.reshape(len(xzk), 1)+zk)))*fk, axis=1)*dzk*fk)*dzk
            form_factors[matk] = FF
        return form_factors
